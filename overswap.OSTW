paramètres
{
	principal
	{
		Description: "Overwatch Hero-Swap Gamemode\r\r\n     Version 2.0.4\n     Made for Anaee (twitch.tv/anaee)\r\r\n     v1.0.0 > 1.4.1 by Tixat\r\r\n     v2.0.0+ by Holdener"
		Nom du mode: "Overswap!"
	}

	salon
	{
		Mettre le jeu en pause en cas de déconnexion d’un joueur: Oui
		Préférence du centre de données: France
		Spectateurs max.: 12
	}

	modes
	{
		Avancée

		Escorte
		{
			cartes activées
			{
			}
		}

		Hybride
		{
			cartes activées
			{
			}
		}

		Général
		{
			Limite de rôles: 1 Tank 2 Dégâts 2 Soutiens
			Règles des parties compétitives: Activé
		}
	}

	la Forge
	{
		Debug mode: Activé
		Hero swap minimum time: 45
	}
}

variables
{
	globale:
		0: hero_swap_time
		1: hero_swap_time_min
		2: hero_swap_time_max
		3: slowmo_duration
		4: slowmo_prct
		5: alert_message_start_time
		6: alert_message_frequency
		7: hero_swap_time_max_host_hud
		8: debug_mode

	de joueur:
		0: target
		1: swap
		2: hero
		3: ult
		4: cooldown
		5: resource
		6: charge
		7: pos
		8: facing
		9: ammo
		10: health
		11: momentum_dir
		12: momentum
}

sous-programmes
{
	0: player_save
	1: hero_swap
	2: abilities_swap
	3: pos_swap
	4: player_set_target
}

règle("debug")
{
	évènement
	{
		Toute la partie - Chaque joueur;
		Les deux;
		Tout;
	}

	conditions
	{
		désactivé Apparition(Joueur exécutant) == Vrai;
		Global.debug_mode == Vrai;
	}

	actions
	{
		Créer du texte d’interface(Joueur exécutant, Chaîne personnalisée("{0}\nIn: {1}", Héros de(Joueur exécutant.target),
			Global.hero_swap_time), Non applicable, Non applicable, Gauche, 0, Couleur(Blanc), Couleur(Blanc), Couleur(Blanc),
			Visible pour et Chaîne de texte, Visibilité par défaut);
		désactivé Définir la description d’objectif(Joueur exécutant, Chaîne personnalisée("{0}\nIn: {1}", Héros de(Joueur exécutant.target),
			Global.hero_swap_time), Visible pour et Chaîne de texte);
	}
}

règle("debug dummy")
{
	évènement
	{
		Toute la partie - Tout le monde;
	}

	conditions
	{
		Global.debug_mode == Vrai;
		Phase de choix de héros == Vrai;
		Nombre de joueurs(Équipe 1) < Nombre d’emplacements(Équipe 1);
		désactivé Apparition(Joueurs dans l’emplacement(0, Équipe 1)) == Vrai;
	}

	actions
	{
		Créer une I.A.(Valeur aléatoire dans le tableau(Tous les héros), Équipe 1, -1, Vecteur(0, 0, 0), Vecteur(0, 0, 0));
		"IA debug spawn"
		Boucle si(Nombre de joueurs(Équipe 1) < Nombre d’emplacements(Équipe 1));
	}
}

règle("init")
{
	évènement
	{
		Toute la partie - Tout le monde;
	}

	actions
	{
		"Debug check"
		Global.debug_mode = Activerdésactiver le paramètre de la Forge(Chaîne personnalisée("Debug options"), Chaîne personnalisée(
			"Debug mode"), Faux, 0);
		"Swap timer message vars"
		Global.alert_message_start_time = 15;
		Global.alert_message_frequency = 5;
		"Slow motion vars"
		Global.slowmo_duration = 1;
		Global.slowmo_prct = 0.500;
		"Hero swap timer vars"
		désactivé Global.hero_swap_time_min = 30;
		Global.hero_swap_time_min = Paramètre entier de la Forge(Chaîne personnalisée("Hero swap timer"), Chaîne personnalisée(
			"Hero swap minimum time"), 30, 5, 120, 0);
		désactivé Global.hero_swap_time_max = 90;
		Global.hero_swap_time_max = Paramètre entier de la Forge(Chaîne personnalisée("Hero swap timer"), Chaîne personnalisée(
			"Hero swap maximum time (defaults to minimum if set to less)"), 90, 5, 120, 1);
		Si(Global.hero_swap_time_max < Global.hero_swap_time_min);
			Global.hero_swap_time_max = Global.hero_swap_time_min;
		Fin;
		"Debug"
		Si(Global.debug_mode);
			Global.hero_swap_time = 20;
		Sinon;
			"Init swap time"
			Global.hero_swap_time = Nombre entier aléatoire(Global.hero_swap_time_min, Global.hero_swap_time_max);
		Fin;
		"Init host hud timer bar max"
		Global.hero_swap_time_max_host_hud = Global.hero_swap_time;
		"Start swap timer"
		Modifier une variable globale selon une cadence(hero_swap_time, 0, 1, Destination et Taux);
	}
}

règle("player_init")
{
	évènement
	{
		Toute la partie - Chaque joueur;
		Les deux;
		Tout;
	}

	actions
	{
		Joueur exécutant.swap = Faux;
	}
}

règle("main loop")
{
	évènement
	{
		Toute la partie - Tout le monde;
	}

	conditions
	{
		Partie en cours == Vrai;
		Global.hero_swap_time <= Global.slowmo_duration / 2;
	}

	actions
	{
		"Slow motion polish"
		Définir un ralenti(100 * Global.slowmo_prct);
		Attendre jusqu’à(Global.hero_swap_time == 0, 99999);
		Attente(Global.slowmo_duration / 2 * Global.slowmo_prct, Ignorer la condition);
		"Debug"
		Si(Global.debug_mode);
			Global.hero_swap_time = 20;
		Sinon;
			"Randomize next swap time"
			Global.hero_swap_time = Nombre entier aléatoire(Global.hero_swap_time_min, Global.hero_swap_time_max);
		Fin;
		"Actualize host hud timer bar max"
		Global.hero_swap_time_max_host_hud = Global.hero_swap_time;
		désactivé Attente(0.500, Ignorer la condition);
		Définir un ralenti(100);
	}
}

règle("player_manager")
{
	évènement
	{
		Toute la partie - Chaque joueur;
		Les deux;
		Tout;
	}

	conditions
	{
		Partie en cours == Vrai;
		Global.hero_swap_time == 0;
	}

	actions
	{
		"Prevent Non Picking"
		Si(!Apparition(Joueur exécutant));
			Forcer un héros(Joueur exécutant, Valeur aléatoire dans le tableau(Héros autorisés(Joueur exécutant)));
			Réapparaître(Joueur exécutant);
		Fin;
		"Set target"
		Sous-programme à appeler(player_set_target);
		"Wait end of action"
		désactivé Attendre jusqu’à(!Utilise Recharger(Joueur exécutant) && !Capacité 1 utilisée(Joueur exécutant) && !Capacité 2 utilisée(
			Joueur exécutant) && !Capacité ultime utilisée(Joueur exécutant), 2);
		"Saving"
		Sous-programme à appeler(player_save);
		"Notifing save, allow swaping to this hero"
		Si(Emplacement de(Joueur exécutant) > 0);
			Joueurs dans l’emplacement(Emplacement de(Joueur exécutant) - 1, Équipe de(Joueur exécutant)).swap = Vrai;
		Sinon;
			Joueurs dans l’emplacement(Décompte de(Tous les joueurs(Équipe de(Joueur exécutant))) - 1, Équipe de(Joueur exécutant))
				.swap = Vrai;
		Fin;
		"Wait for all player_save"
		Attendre jusqu’à(Joueur exécutant.swap, 99999);
		"Prevent abilities until cooldown load"
		Interdire le bouton(Joueur exécutant, Bouton(Tir principal));
		Interdire le bouton(Joueur exécutant, Bouton(Tir secondaire));
		Interdire le bouton(Joueur exécutant, Bouton(Capacité 1));
		Interdire le bouton(Joueur exécutant, Bouton(Capacité 2));
		"Hero Swap"
		Sous-programme à appeler(hero_swap);
		Joueur exécutant.swap = Faux;
		"Death Check (IN TEST for DVA BUG 2nd condition)"
		Si(Joueur exécutant.target.health == 0 || En vie(Joueur exécutant.target) == Faux);
			Réapparaître(Joueur exécutant);
		Sinon;
			"Position Swap"
			Lancer la règle(pos_swap, Ne rien faire);
		Fin;
		"Sound Effect"
		Jouer un effet(Joueur exécutant, Son d’impact d’amélioration, Couleur(Blanc), Joueur exécutant, 100);
		"Wait for hero swap (can lower it to 0.016 ?)"
		Attente(0.016 * Global.slowmo_prct, Ignorer la condition);
		"Cooldown swap"
		Sous-programme à appeler(abilities_swap);
		Autoriser un bouton(Joueur exécutant, Bouton(Tir principal));
		Autoriser un bouton(Joueur exécutant, Bouton(Tir secondaire));
		Autoriser un bouton(Joueur exécutant, Bouton(Capacité 1));
		Autoriser un bouton(Joueur exécutant, Bouton(Capacité 2));
		Attendre jusqu’à(Global.hero_swap_time > 0, 99999);
	}
}

règle("player_set_target")
{
	évènement
	{
		Sous-programme;
		player_set_target;
	}

	actions
	{
		"Set Target"
		Si(Emplacement de(Joueur exécutant) < Décompte de(Tous les joueurs(Équipe de(Joueur exécutant))) - 1);
			Joueur exécutant.target = Joueurs dans l’emplacement(Emplacement de(Joueur exécutant) + 1, Équipe de(Joueur exécutant));
		Sinon;
			Joueur exécutant.target = Joueurs dans l’emplacement(0, Équipe de(Joueur exécutant));
		Fin;
	}
}

règle("player_save")
{
	évènement
	{
		Sous-programme;
		player_save;
	}

	actions
	{
		"Save hero"
		Joueur exécutant.hero = Héros de(Joueur exécutant);
		"Save ult"
		Joueur exécutant.ult = Pourcentage de charge de la capacité ultime(Joueur exécutant);
		"Save Abilities Cooldown"
		Joueur exécutant.cooldown[0] = Temps de recharge de la capacité(Joueur exécutant, Bouton(Tir principal));
		Joueur exécutant.cooldown[1] = Temps de recharge de la capacité(Joueur exécutant, Bouton(Tir secondaire));
		Joueur exécutant.cooldown[2] = Temps de recharge de la capacité(Joueur exécutant, Bouton(Capacité 1));
		Joueur exécutant.cooldown[3] = Temps de recharge de la capacité(Joueur exécutant, Bouton(Capacité 2));
		"Save Abilities Ressource"
		Joueur exécutant.resource[0] = Ressource de la capacité(Joueur exécutant, Bouton(Tir principal));
		Joueur exécutant.resource[1] = Ressource de la capacité(Joueur exécutant, Bouton(Tir secondaire));
		Joueur exécutant.resource[2] = Ressource de la capacité(Joueur exécutant, Bouton(Capacité 1));
		Joueur exécutant.resource[3] = Ressource de la capacité(Joueur exécutant, Bouton(Capacité 2));
		Joueur exécutant.resource[4] = Ressource de la capacité(Joueur exécutant, Bouton(Sauter));
		"Save Abilities Charge"
		Joueur exécutant.charge[0] = Charge de la capacité(Joueur exécutant, Bouton(Tir principal));
		Joueur exécutant.charge[1] = Charge de la capacité(Joueur exécutant, Bouton(Tir secondaire));
		Joueur exécutant.charge[2] = Charge de la capacité(Joueur exécutant, Bouton(Capacité 1));
		Joueur exécutant.charge[3] = Charge de la capacité(Joueur exécutant, Bouton(Capacité 2));
		"Save position"
		Joueur exécutant.pos = Position de(Joueur exécutant);
		"Save facing"
		Joueur exécutant.facing = Regard en direction de(Joueur exécutant);
		"Save ammo"
		Joueur exécutant.ammo[0] = Munitions(Joueur exécutant, 0);
		Joueur exécutant.ammo[1] = Munitions(Joueur exécutant, 1);
		"Save health"
		Joueur exécutant.health = Points de vie(Joueur exécutant);
		"Save momentum (thanks workshop.codes/4G0ZB)"
		Joueur exécutant.momentum_dir = Vecteur(Vitesse dans la direction donnée de(Joueur exécutant, Vecteur(1, 0, 0)),
			Vitesse dans la direction donnée de(Joueur exécutant, Vecteur(0, 1, 0)), Vitesse dans la direction donnée de(Joueur exécutant,
			Vecteur(0, 0, 1)));
		Joueur exécutant.momentum = Vitesse dans la direction donnée de(Joueur exécutant, Normalisation(Joueur exécutant.momentum_dir));
	}
}

règle("hero_swap")
{
	évènement
	{
		Sous-programme;
		hero_swap;
	}

	actions
	{
		"Heroes swap"
		Arrêter de forcer un héros(Joueur exécutant);
		Forcer un héros(Joueur exécutant, Joueur exécutant.target.hero);
		Arrêter de forcer un héros(Joueur exécutant);
	}
}

règle("pos_swap")
{
	évènement
	{
		Sous-programme;
		pos_swap;
	}

	actions
	{
		Téléportation(Joueur exécutant, Joueur exécutant.target.pos);
		Définir la direction du regard(Joueur exécutant, Joueur exécutant.target.facing, Au monde);
		"Restore momentum (thanks workshop.codes/4G0ZB)"
		Appliquer une impulsion(Joueur exécutant, Normalisation(Joueur exécutant.target.momentum_dir), Joueur exécutant.target.momentum,
			Au monde, Annuler le mouvement contraire);
		désactivé Attente(0.250, Ignorer la condition);
	}
}

règle("abilities_swap")
{
	évènement
	{
		Sous-programme;
		abilities_swap;
	}

	actions
	{
		"Ult swap"
		Définir la charge de la capacité ultime(Joueur exécutant, Joueur exécutant.target.ult);
		"Abilities Cooldown"
		Définir le temps de recharge de la capacité(Joueur exécutant, Bouton(Tir principal), Joueur exécutant.target.cooldown[0]);
		Définir le temps de recharge de la capacité(Joueur exécutant, Bouton(Tir secondaire), Joueur exécutant.target.cooldown[1]);
		Définir le temps de recharge de la capacité(Joueur exécutant, Bouton(Capacité 1), Joueur exécutant.target.cooldown[2]);
		Définir le temps de recharge de la capacité(Joueur exécutant, Bouton(Capacité 2), Joueur exécutant.target.cooldown[3]);
		"Abilities Ressource"
		Définir la ressource de la capacité(Joueur exécutant, Bouton(Tir principal), Joueur exécutant.target.resource[0]);
		Définir la ressource de la capacité(Joueur exécutant, Bouton(Tir secondaire), Joueur exécutant.target.resource[1]);
		Définir la ressource de la capacité(Joueur exécutant, Bouton(Capacité 1), Joueur exécutant.target.resource[2]);
		Définir la ressource de la capacité(Joueur exécutant, Bouton(Capacité 2), Joueur exécutant.target.resource[3]);
		Définir la ressource de la capacité(Joueur exécutant, Bouton(Sauter), Joueur exécutant.target.resource[4]);
		"Abilities Charge"
		Définir la charge de la capacité(Joueur exécutant, Bouton(Tir principal), Joueur exécutant.target.charge[0]);
		Définir la charge de la capacité(Joueur exécutant, Bouton(Tir secondaire), Joueur exécutant.target.charge[1]);
		Définir la charge de la capacité(Joueur exécutant, Bouton(Capacité 1), Joueur exécutant.target.charge[2]);
		Définir la charge de la capacité(Joueur exécutant, Bouton(Capacité 2), Joueur exécutant.target.charge[3]);
		"Ammo"
		Définir les munitions(Joueur exécutant, 0, Joueur exécutant.target.ammo[0]);
		Définir les munitions(Joueur exécutant, 1, Joueur exécutant.target.ammo[1]);
		"Health"
		Si(Joueur exécutant.target.health != 0);
			Définir les points de vie d’un joueur(Joueur exécutant, Joueur exécutant.target.health);
		Fin;
	}
}

règle("hero swap alert")
{
	évènement
	{
		Toute la partie - Chaque joueur;
		Les deux;
		Tout;
	}

	conditions
	{
		Arrondir à l’entier(Global.hero_swap_time, Au plus près) <= Global.alert_message_start_time;
		Arrondir à l’entier(Global.hero_swap_time, Au plus près) % Global.alert_message_frequency == 0;
	}

	actions
	{
		Sous-programme à appeler(player_set_target);
		Si(Arrondir à l’entier(Global.hero_swap_time, Au plus près) >= Global.alert_message_frequency);
			Message en petit(Joueur exécutant, Chaîne personnalisée("Swapping to {0} in {1}s", Chaîne d’icône du héros(Héros de(
				Joueur exécutant.target)), Arrondir à l’entier(Global.hero_swap_time, En dessous)));
		Sinon;
			Message en grand(Joueur exécutant, Chaîne personnalisée("Swapping to {0}", Chaîne d’icône du héros(Héros de(
				Joueur exécutant.target))));
		Fin;
		Attendre jusqu’à(Arrondir à l’entier(Global.hero_swap_time, Au plus près) % Global.alert_message_frequency != 0, 2);
	}
}

règle("hud")
{
	évènement
	{
		Toute la partie - Tout le monde;
	}

	actions
	{
		"swap timer bar"
		Créer du texte d’interface de barre de progression(Tous les joueurs(Toutes les équipes),
			Global.hero_swap_time / Global.hero_swap_time_max_host_hud * 100, Chaîne personnalisée("Next swap: {0}", Arrondir à l’entier(
			Global.hero_swap_time, Au-dessus)), Gauche, 1, Couleur(Citron vert), Couleur(Blanc), Visible pour Valeurs et Couleur,
			Toujours visible);
	}
}